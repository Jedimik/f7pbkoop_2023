﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CV04
{
    public class Animal
    {
        public string Name { get; private set; }
        public int NumOfLegs { get; protected set; }
        public string Color { get; protected set; }

        public string Race { get; protected set; }

        public virtual string Species { get; protected set; }
        public Animal(string Name, int NumOfLegs,string Color,string Race)
        {
            this.Name = Name;
            this.NumOfLegs = NumOfLegs;
            this.Color = Color;
            this.Race = Race;
            Init();
        }

        public Animal(string Name, string Color, string Race)
        {
            this.Name = Name;
            this.Color = Color;
            this.Race = Race;
            Init();
        }

        private void Init()
        {
            this.Species = "DajakeZvire";
        }


        public string GetSome()
        {
            return "Ahoj Svete";
        }

        public virtual string GetSomething()
        {
            return "Jsem zvire";
        }


        public override string ToString()
        {
            return $"Zvire se jmenem:{Name} a poctem nohou:{NumOfLegs}" +
                $" a barvou:{Color} je rasy:{Race}";
        }
    }
}

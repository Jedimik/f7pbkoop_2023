﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CV04
{
    public class Dog:Animal
    {
        public bool IsTrained { get; private set; }
         string Species = "Pes";
        public Dog(string Name, int NumOfLegs, string Color, string Race,bool IsTrained)
            :base(Name:Name, Color:Color, Race:Race)
        {
            this.IsTrained= IsTrained;
            this.NumOfLegs= NumOfLegs+10;
            //this.Species = "Pes";
        }

        //public override string GetSome()
        //{
        //    return "Hello world";
        //}

        public override string GetSomething()
        {
            return "Jsem pes";
        }

        public override string ToString()
        {
            string outp="";
            if (this.IsTrained) outp = "je vycviceny";
            else outp = "neni vycviceny a zlobi jak cert";
            return $"Zvire druhu:{Species} {base.ToString()} {outp}";
        }

    }
}

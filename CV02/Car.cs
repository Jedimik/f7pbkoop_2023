﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CV02
{
    public  class Car
    {
        public string color { get; private set; }
        public string brand { get; private set; }

        public int mileage { get; private set; }

        public int price;


        public Car(string color, string brand, int mileage, int price)
        {
            this.color = color;
            this.brand = brand;
            this.mileage = mileage;
            this.price = price;
        }

        public override string ToString()
        {
            return $"Auto barvy:{this.color} znacky:{this.brand} ma najeto:{this.mileage}" +
                $" a stoji:{this.price}.";
        }

        public void RaisePrice(int val)
        {
            double multiplier = (val / 100);
            this.price += Convert.ToInt32(this.price*multiplier);
        }
        public void DecreasePrice(int val)
        {
            double multiplier = (val / 100);
            this.price += Convert.ToInt32(this.price * multiplier);
        }
        public void ManagePrice(int val)
        {
            double multiplier = (val / 100F);
            this.price += Convert.ToInt32(this.price * multiplier);
        }



    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CV02
{
    public class Animal
    {

        // Parametry / Atributy
        public string type { get; private set; }
        public string name;
        public int age { get; private set; }
        private int n_legs;

        // Konstruktor
        public Animal(string type, string name, int age)
        {
            this.type = type;
            this.name = name;
            this.age = age;
            this.n_legs = 4;
        }

        // Metody
        // Narozeninova metoda
        public void Birthday()
        {
            this.age += 1;
            Console.WriteLine($"Gratuluji ti {this.name} dnes ti je uz: {this.age} let");
        }

        public int ReturnLegs()
        {
            return this.n_legs - 2;
        }
    }
}

﻿using CV02;

internal class Program
{

    private static void Main(string[] args)
    {
        Console.WriteLine("Hello, World!");
        Animal animal1 = new Animal("pes", "Alik", 5);
        Console.WriteLine(animal1.type);
        Console.WriteLine(animal1.ReturnLegs());

        Car auto = new Car("cervena", "oppel",
            101101, 100000);
        Console.WriteLine(auto.ToString());
        auto.ManagePrice(-8);
        Console.WriteLine(auto.ToString());
        Console.WriteLine(auto.ToString());
        // Zmenit barvu, zmenit tachometr zmenit cenu
        List<Car> car_list = new List<Car>();
        car_list.Add(auto);
        car_list.Add(new Car(
            "cerne", "toyota", 12000, 838382));
        Console.WriteLine($"Auto1:{car_list[0]}\n" +
            $"Auto2;{car_list[1]}");

        string[] colorArr = { "cerna", "modra","zluta","zelena"};
        List<string> brandlist = new List<string>();
        brandlist.Add("oppel");
        brandlist.Add("mercedes");
        // Vygenerujte 10 nahodnych aut a pridejte do listu
        int a = 10;
        for(int i=0;i<a;i++)
        {
            Random rnd = new Random();
            car_list.Add(new Car(colorArr[rnd.Next(0, colorArr.Length)],
                brandlist[rnd.Next(0, brandlist.Count)], rnd.Next(10000, 250000), rnd.Next(60000, 1500000)));
        }

        for(int i=0;i<car_list.Count; i++) { Console.WriteLine(car_list[i].ToString()); }



    }
}

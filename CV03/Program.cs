﻿namespace CV03
{
    internal class Program
    {
        static void Main(string[] args)
        {



            //Factory stratus= new Factory();
            //stratus.AddCar(new Car(year: 2011, brand: "oppel", color: "green"));
            //stratus.AddCar(new Car("hyundai", 2012, "yellow"));
            //stratus.CreateCar(brand: "Ford", color: "red");
            //Console.WriteLine(stratus.ToString());

            Fighter f1 = new Fighter("Ptacek z Chebu", 20, 8, 150, 6);
            Fighter f2 = new Fighter("Svarta JemuHodne", 18, 14, 170, 8);
            Fight f = new Fight(f1, f2);
            f.StartFight();

        }
    }
}
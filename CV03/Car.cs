﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CV03
{
    public class Car
    {
        // Parametry
        public string brand { get; private set;}
        public int year { get; private set;}
        public string color { get; private set;}
        // Konstruktor
        public Car(string brand,int year, string color)
        {
            this.brand = brand;
            this.year = year;
            this.color = color;
        }
        // Oveeride ToString metody
        public override string ToString()
        {
            return $"Auto znacky:{brand} bylo vyrobeno v roce:{year} a je barvy:{color}";
        }

    }
}

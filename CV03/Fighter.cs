﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CV03
{
    public class Fighter
    {
        public string Name { get; private set; }
        public int Atk { get; private set; }
        public int Deff { get; private set; }
        public int Hp { get; private set; }
        public int Int { get; private set; }

        public Fighter(string Name,int Atk, int Deff, int Hp, int Int)        {
            this.Name = Name;
            this.Atk = Atk;
            this.Deff = Deff;
            this.Hp = Hp;
            this.Int = Int;
        }

        public int GenAttack()
        {
            return new Random().Next(1,Atk);
        }

        public int GenDefense() 
        {
            return new Random().Next((int)Deff/4,Atk);
        }

        public int GenInt()
        {
            return new Random().Next((int)(Int / 2), Int);
        }

        public void ChangeHP(int value)
        {
            Hp += value;
        }

        public override string ToString()
        {
            return $"Bojovnik:{Name} ma silu:{Atk} deffenzivu:{Deff} ma IQ:{Int} a ma:{Hp} zivotu.";
        }



    }
}

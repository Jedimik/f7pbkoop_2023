﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CV03
{
    public class Factory
    {
        public List<Car> carsList {  get; private set; }
        public int year { get;private set; }

        public Factory(int year=2020) {
            this.year = year;
            carsList = new List<Car>();
        }

        public void AddCar(Car car)
        {
            carsList.Add(car);
        }

        public void CreateCar(string color,string brand)
        {
            carsList.Add(new Car(brand: brand, color: color, year: this.year));
        }

        public override string ToString()
        {
            string s = "\n";
            foreach(Car car in carsList)
            {
                s+=$"{car.ToString()}\n";
            }
            return s; 
                
        }


    }
}

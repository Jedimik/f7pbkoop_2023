﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CV03
{
    public  class Fight
    {

        public Fighter f1 {  get; private set; }
        public Fighter f2 { get; private set; }

        public Fight(Fighter f1, Fighter f2)
        {
            this.f1 = f1;
            this.f2 = f2;
        }

        public void StartFight()
        {
            //int tmp = 0;
            Fighter winner = Simulate();
            Console.WriteLine($"Souboj vyhral:{winner.ToString()}");
        }

        public Fighter Simulate()
        {
            while (f1.Hp > 0 && f2.Hp > 0)
            {
                //Kdo utoci na zaklade inteligence
                if (f1.GenInt() > f2.GenInt())
                { AttackRound(f1, f2); }
                else { AttackRound(f2, f1); }

            }
            if (f1.Hp <= 0)
            { return f2; }
            return f1;
        }

        public void AttackRound(Fighter f1, Fighter f2) //Utocici F1 brani se F2
        {
            int f1_atk = f1.GenAttack();
            int f2_deff = f2.GenDefense();
            if(f1_atk>f2_deff)
            {
                f2.ChangeHP(-f1_atk);
                Console.WriteLine($"Bojovnik:{f1.Name} zautocil silou:{f1_atk} na hrace:{f2.Name}, ktery se branil" +
                    $" zuby nechty za:{f2_deff} a nyni to vydejchava s:{f2.Hp} zivotu.");
                return;
            }
            Console.WriteLine($"Bojovnik:{f1.Name} zautocil silou:{f1_atk} na hrace:{f2.Name}, ktery se mrstne vyhnul utoku" +
    $" a stale mu zustava:{f2.Hp} zivotu.");
        }

    }
}

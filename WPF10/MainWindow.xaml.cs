﻿using MySqlConnector;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPF10
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static string projectDirectory = Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).Parent.Parent.Parent.FullName;
        private List<User> userList;
        private DBconnect db;
        public MainWindow()
        {
            userList= new List<User>();
            InitializeComponent();
            ConfigLoader config = new ConfigLoader($"{projectDirectory}\\configs\\dbConfig.json");
            db = new DBconnect(config);
        }

        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            MySqlDataReader output = db.Select("SELECT RoleID, name FROM role");
        }
    }
}

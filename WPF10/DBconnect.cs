﻿using MySqlConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPF10
{
    public class DBconnect
    {
        private string Mysqlconn;
        public MySqlConnection connection;
        public DBconnect()
        {
            Mysqlconn = "Server=;Database=;Uid=;Pwd=;";
        }
        public DBconnect(string server, string database, string uid, string pwd, bool datetime_convert=true)
        {
            Mysqlconn = String.Format("Server={0};Database={1};Uid={2};Pwd={3};Convert Zero Datetime={4};", server, database, uid, pwd, datetime_convert);
        }

        public DBconnect(ConfigLoader cnf)
        {
            bool datetime_convert;
            try
            {
                datetime_convert = (bool) cnf.getValue("datetime_convert");
            }
            catch { datetime_convert = false; }
            Mysqlconn = String.Format("Server={0};Database={1};Uid={2};Pwd={3};Convert Zero Datetime={4};", cnf.getValue("host"),cnf.getValue("database") ,cnf.getValue("username"),cnf.getValue("password"),datetime_convert);
        }

        public MySqlDataReader Select(string command) // Prepsat, aby to pak vracelo genericky slovnik
        {
            this.connection = new MySqlConnection(Mysqlconn);
            MySqlCommand cmd = new MySqlCommand(command, connection);
            connection.Open();
            MySqlDataReader reader = cmd.ExecuteReader();
            return reader;
        }
        public void Insert(string command)
        {
            this.connection = new MySqlConnection(Mysqlconn);
            MySqlCommand cmd = new MySqlCommand(command, connection);
            connection.Open();
            MySqlDataReader reader = cmd.ExecuteReader();
            CloseConn();
        }
        public void CloseConn()
        {
            this.connection.Close();
        }

        public void Update(string command)
        {
            this.connection = new MySqlConnection(Mysqlconn);
            MySqlCommand cmd = new MySqlCommand(command, connection);
            connection.Open();
            MySqlDataReader reader = cmd.ExecuteReader();
            CloseConn();
        }

        public void Delete(string command)
        {
            this.connection = new MySqlConnection(Mysqlconn);
            MySqlCommand cmd = new MySqlCommand(command, connection);
            connection.Open();
            MySqlDataReader reader = cmd.ExecuteReader();
            CloseConn();
        }
    }
}

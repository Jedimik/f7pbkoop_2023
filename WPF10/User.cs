﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPF10
{
    public class User
    {
        public int id { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }

        public User(int id, string firstname, string lastname)
        {
            this.id = id;
            this.firstname = firstname;
            this.lastname = lastname;
        }
        public override string ToString()
        {
              return $"User: {firstname} {lastname}";
        }

        public bool equals(User user)
        {
            return this.id == user.id;
        }
    }
}

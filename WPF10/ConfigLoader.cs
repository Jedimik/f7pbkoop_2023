﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json.Serialization;
using Newtonsoft.Json;
namespace WPF10
{
    public class ConfigLoader
    {
        public Dictionary<string,object> dictionary { get; private set; }
        public ConfigLoader(string path) {
            load_config(path);
        }

        private void load_config(string path)
        {
            string content = File.ReadAllText(path);
            this.dictionary = JsonConvert.DeserializeObject<Dictionary<string, object>>(content);

            if (this.dictionary == null)
            {
                throw new Exception($"Cannot deserialize config file from: {path}");
            }
        }

        public object getValue(string key)
        {
            return dictionary[key];
        }
        
        public void setValue(string key, object value)
        {
            dictionary[key] = value;
        }

    }
}

﻿using MySqlConnector;

namespace CV_10_Consol
{
    public class Program
    {
        static string projectDirectory = Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).Parent.Parent.Parent.FullName;
        static void Main(string[] args)
        {
            string output=$"{projectDirectory}\\configs\\dbConfig.json";
            ConfigLoader cnf = new ConfigLoader(output);
            DBconnect db = new DBconnect(cnf);
            MySqlDataReader reader = db.Select("SELECT firstname,lastname FROM user");
            while (reader.Read())
            {
                Console.WriteLine($"{reader.GetString(0)} {reader.GetString(1)}");
            }
            
        }
    }
}
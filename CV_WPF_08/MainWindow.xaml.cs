﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CV_WPF_08
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ObservableCollection<Dog> dogList;
        public MainWindow()
        {
            InitializeComponent();
            dogList = new ObservableCollection<Dog>();
            lb.ItemsSource = dogList;
        }
        // Vytvorte tridu pes, pes ma jen jmeno. + metodu pro zmenu jmena
        // Pridejte psa podle jmena to listboxu (tlacitkem 1) / jmeno se generuje na zaklade hodnoty v textboxu
        // Pracujte s druhym tlacitkem tak, abyste mohli menit jmeno psa a zaroven se vam to propsalo do listboxu
        // Jmeno to nacita z puvodniho tetxboxu.

        private void btn0_Click(object sender, RoutedEventArgs e)
        {
            //lb.Items.Add(tb.Text);
            //lv.Items.Add(tb.Text);
            //cb.Items.Add(tb.Text);
            dogList.Add(new Dog(name: tb.Text));
        }

        private void btn1_Click(object sender, RoutedEventArgs e)
        {
            if (lb.SelectedItem is Dog selectedObject)
            {
                foreach(Dog dog in dogList)
                {
                    if (dog==selectedObject)
                    {
                        dog.name = tb.Text;
                    }
                }
                //lbl.Content = selectedObject.name.ToString();
                lb.Items.Refresh();
            }
        }
    }
}

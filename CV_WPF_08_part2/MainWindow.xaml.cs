﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace CV_WPF_08_part2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private DispatcherTimer timer;
        bool smer = false;
        public MainWindow()
        {
            InitializeComponent();
            timer = new DispatcherTimer(DispatcherPriority.Render);
            //timer.Interval = TimeSpan.FromMilliseconds(16); // Adjust the interval as needed
            timer.Interval = new TimeSpan(0, 0, 0, 0, 10);
            timer.Tick += Timer_Tick;
        }

        private void Timer_Tick(object? sender, EventArgs e)
        {
            if (!smer)
            {
                Canvas.SetLeft(cnv.Children[0], Canvas.GetLeft(cnv.Children[0]) + 5);
            }
            else { Canvas.SetLeft(cnv.Children[0], Canvas.GetLeft(cnv.Children[0]) - 5); }
            if (Canvas.GetLeft(cnv.Children[0])+ cnv.Children[0].RenderSize.Width > cnv.MaxWidth)
            {
                smer= true;
            }
            else if(Canvas.GetLeft(cnv.Children[0]) + cnv.Children[0].RenderSize.Width < cnv.MinWidth)
            {
                smer = false;
            }
            // Jakmile se rectangle dotkne konce, meni smer a pluje zpatky, pak se zase odrazi a zase opacnym smerem.
        }

        // Tlacitko pridava rectangle do platna na random pozici
        private void btn0_Click(object sender, RoutedEventArgs e)
        {
            Rectangle rec = new Rectangle() {
                Height = 20,
                Width = 40,
                Fill=new SolidColorBrush(Color.FromRgb(255, 0, 0)),
            };
            Canvas.SetTop(rec, 10);
            Canvas.SetLeft(rec, 10);
            cnv.Children.Add(rec);            

        }
        // Druhy tlacitko posouva rectangle o 10 bodu doprava pri kazdem stistku

        private void btn1_Click(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < cnv.Children.Count; i++)
            {
                Canvas.SetLeft(cnv.Children[i], Canvas.GetLeft(cnv.Children[i]) + 10);
            }
        }

        private void btn2_Click(object sender, RoutedEventArgs e)
        {
            timer.Start();
        }
    }
}

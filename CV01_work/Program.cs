﻿using System;

namespace CV01_work
{
    enum Fruit
    {
        Mango,
        Lemon,
        Apple,
        Orange
    }

    class Program
    {
        // Funkce bez navatove hodnoty
        static void NapisAhoj(char sign='!')
        {
            Console.WriteLine($"Ahoj Svete{sign}");
        }
        static void Napis(string a, string b, string c="Buldozer",char sign = '!')
        {
            Console.WriteLine($"Ahoj Svete{sign} {a} {b} {c}");
        }
        // Funkce s navratovou hodnotou
        static int Soucet(int a, int b)
        { return a + b; }
        static int Rozdil(int a, int b)
        { return a - b; }
        static int Soucin(int a, int b)
        { return a * b; }
        static int Podil(int a, int b)
        { return a / b; }

        //        funkce  pro +,-,*,/
        // Rekurze
        static void Rek(int a)
        {
            if(a<=0)
            {
                return;
            }
            Console.Write($"{a} ");
            Rek(a/2);
            Rek(a/2);
        }
        static void Main(string[] args)
        {
            //Enum
            Console.WriteLine(Fruit.Lemon);
            Rek(6);
            // Pole
            /*
            int[] arr= new int[5];
            int[] arr2 = { 0, 1, 2, 3, 4, 4 };
            int[,] arr2D = { { 0, 1, 2 }, { 3, 4, 5 }, { 6, 7, 8 } };
            for(int i=0;i<arr2.Length;i++)
            { Console.WriteLine($"{arr2[i]}"); }
            for (int i = 0; i < arr2D.GetLength(0); i++)
            { 
                for(int j=0;j<arr2D.GetLength(1);j++)
                {
                    Console.Write($"{arr2D[i, j]}");
                }
                Console.Write("\n");
            }
            */
            // Funkce
            /*
            NapisAhoj('@');
            Napis("Ahoj", "ne");
            */
            // S navratovou hodnotou
            /*
            int a = 3; int b = 2; int c = 4;
            int vysledek1; int vysledek2; int vysledek3;
            vysledek1 = Soucin(Rozdil(a, b), c);
            vysledek2 = Rozdil(Soucet(a, b), Soucin(c, a));
            vysledek3 = Podil(Soucet(Soucin(Rozdil(a,b),c),Soucet(a,b)),Soucet(Soucin(c,Rozdil(a,b)),b));
            Console.WriteLine($"Vysledek1:{vysledek1} Vysledek2:{vysledek2} Vysledek3:{vysledek3}");
            */
            // Datove typy promenne
            /*
            int varA;
            double varB;
            char bf = 'b';
            //string s = "Ahoj Svete!";            
            bool bo = true;
            */
            /*
            Console.WriteLine(bf);
            Console.Write(s);
            Console.Write(s);*/
            // Formatovani stringu
            /*
            string ahoj = "Ahoj";
            string svet = "Svete";
            s = "Ahoj" + " " + "Svete" + "!";
            Console.WriteLine(s);
            s = ahoj + " " + svet + "!";
            s = String.Format("{1} {0}!3", svet, ahoj);
            s = $"{ahoj} {svet}!4";
            Console.WriteLine(s);*/
            // Nacteni vstupu
            /* Console.Write("Napis mi tvoje jmeno:");
             string jmeno = Console.ReadLine();
             Console.WriteLine($"Zadal jsi opravdu toto jmeno? Jmeno:{jmeno}");*/
            // Cykly
            /*for(int i=0;i<10;i++)
            {
                Console.WriteLine($"{i}");
                //Console.WriteLine(i);
            }
            int r = 10;
            int s = 5;
            for (int i=0;i<r;i++)
            {
                for(int j=0;j<s;j++)
                {
                    if(i%2==0)
                    {
                        Console.Write($"{j+1}");
                    }
                    else
                    {
                        Console.Write($"{(s-j)}");
                    }
                }
                Console.WriteLine();
            }
            */
            // While cyklus
            /*
            while(true)
            {
                Console.WriteLine("Hadej znak, ktery to ukonci");
                char b = Console.ReadKey().KeyChar;
                if (b == 'd') { break; }
            }*/
            /*
            int x = -1;
            do
            {
                Console.WriteLine($"Jakto, ze jedu, kdyz v xku je hodnota:{x}");

            } while (x > 0);
            */







        }
    }
}

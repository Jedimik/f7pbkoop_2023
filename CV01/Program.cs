﻿using System;
using System.Text;
using System.Xml.Linq;

namespace CV01
{
    internal class Program
    {
        enum Level
        {
            Low,
            Medium,
            High
        }

        static void vypis()
        {
            Console.WriteLine("Ahojda");
        }
        static void zmen(int a,int b=3)
        {
            a = b;
        }

        static void zmen2(ref int a, int b = 3)
        {
            a = b;
        }

        static int zmen3(int a, int b = 3)
        {
            return a = b;
        }
        static void Main(string[] args)
        {
            /* 
             Promenne
            Vypisovani, formatovane vypisovani
            Nacteni vstupu
            Cykly
            Funkce - void/return
            pole/listy
            enum
            rekurze
             */

            // promenne
            int a = 5;
            double b = 5.4;
            bool boolean = true;
            string s = "Ahoj";
            char c = 'b';

            // String formating
            string s1 = s + " Svete!";
            string s2 = String.Format("Hodnota v a je:{0}", a);
            string s3 = $"V promenne b je hodnota:{b}";
            StringBuilder sb = new StringBuilder();
            string name = "Svete!";
            sb.Append("Hello, ");
            sb.Append(name);
            sb.Append("!");
            string message = sb.ToString();
            DateTime now = DateTime.Now;
            string date = now.ToString("MM/dd/yyyy");  // Outputs something like 09/26/2023


            Console.WriteLine(s1+"\n"+ s2 + "\n" + s3 + "\n" + name + "\n" + message + "\n" + date);

            string result = string.Join("\n","\nThis is new one", s1, s2, s3, name, message, date);
            Console.WriteLine(result);
            // Nacteni vstupu
            Console.Write("Napis mi tve jmeno:");
            string output= Console.ReadLine();
            Console.WriteLine("\nNapsal jsi toto jmeno:{0}", output);
            Console.Write("Napis mi jeden znak:");
            char sign = Console.ReadKey().KeyChar;
            Console.WriteLine("\nByl to tento znak:{0}", sign);

            while( a > 0){
                Console.WriteLine("A je rovno:{0}", a);
                a--;
            }
            do
            {
                Console.WriteLine("A je rovno:{0}", a);
                a--;
            } while(a>0);
            for(int i=0;i<5;i++)
            {
                Console.WriteLine("Hodnota iterace je i:{0}", i);
            }

            int test = 2;
            zmen(test);
            Console.WriteLine("Zmenena hodnota s void hodnotou je:{0}",test);
            zmen2(ref test);
            Console.WriteLine("Zmenena hodnota s referenci je:{0}", test);
            test =zmen3(test, 6);
            Console.WriteLine("Zmenena hodnota s return hodnotou je:{0}", test);

            int[] x = new int[5];
            int[] xy = { 0, 3, 4 };
            int[,] arr2D = { { 0, 1, 2 }, { 3, 4, 5 } };

            Console.WriteLine("Enumone:{0}", Level.Low);
            Console.WriteLine("Enumone:{0}", (int)Level.Low);
        }
    }
}
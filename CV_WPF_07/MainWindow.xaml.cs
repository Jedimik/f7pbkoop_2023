﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using static System.Net.Mime.MediaTypeNames;

namespace CV_WPF_07
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }



        // udeljte pocitadlo, kazdy klik +1
        // Pouzijte 2 TB pro cisla, a vytvorte tlacitko pro jejich soucet, vypiste jej v labelu nebo textblocku.
        private void btn0_Click(object sender, RoutedEventArgs e)
        {
            lbl0.Content = tb0.Text;
        }

        // Pomoci parametru visibility errorovy label, ktery bude vetsim pismem, cervene.
        // Pokud uzivatel chce zadat neco jineho nez cislo, resp. hodi nam to vyjimku, pokud budeme chtit parsovat
        // Pak to napise errorovou hlasku
        // Samozrejme, pokud to napiseme dobre, hlaska se nezobrazuje.
        private void btnSum_Click(object sender, RoutedEventArgs e)
        {
            tblCalc.Text = $"{double.Parse(tbA.Text) + double.Parse(tbB.Text)}";
        }
        private void btnSum1_Click(object sender, RoutedEventArgs e)

        {
            if (hideErrorMessage())
            {
                Result.Text = $"{double.Parse(Result.Text) + double.Parse(input.Text)}";
            }
        }

        private void btnMultiply_Click(object sender, RoutedEventArgs e)
        {
            if (hideErrorMessage())
            {
                Result.Text = $"{double.Parse(Result.Text) + double.Parse(input.Text)}";
            }
        }

        private void btnMinus_Click(object sender, RoutedEventArgs e)
        {
            if (hideErrorMessage())
            {
                Result.Text = $"{double.Parse(Result.Text) + double.Parse(input.Text)}";
            }
        }

        private void btnDivide_Click(object sender, RoutedEventArgs e)
        {
            if (hideErrorMessage() && checkDivideZero())
            {
                Result.Text = $"{double.Parse(Result.Text) + double.Parse(input.Text)}";
            }
        }
        // Vytvorte kompletni kalkulacku pro +,-,*,/,%

        private void setErrorMessage(string message)
        {
            lblError.Content = message;
            lblError.Visibility = Visibility.Visible;
        }
        private bool hideErrorMessage()
        {
            lblError.Visibility = Visibility.Hidden;
            if (!NumberValidationTextBox(input.Text))
            {
                setErrorMessage("You can use only numbers");
                return false;
            }
            return true;
        }

        private bool NumberValidationTextBox(string text)
        {
            Regex regex = new Regex("[^0-9]+");
            return !regex.IsMatch(text);
        }

        private bool checkDivideZero()
        {
            if(double.Parse(input.Text).Equals(0))
            {
                setErrorMessage("You cannot divide by 0");

                return false;
            }
            return true;
        }


    }
}

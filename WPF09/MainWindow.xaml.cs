﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace WPF09
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public List<Dog> dogList = new List<Dog>();
        private DispatcherTimer timer;
        private List<Elipsa> elipsList= new List<Elipsa>();
        public MainWindow()
        {
            InitializeComponent();
            timer = new DispatcherTimer(DispatcherPriority.Render);
            timer.Interval = new TimeSpan(0, 0, 0, 0, 10);
            timer.Tick += Timer_Tick;

        }

        private void Timer_Tick(object? sender, EventArgs e)
        {
            foreach (var o in elipsList)
            {
                o.X = (int)Canvas.GetLeft(o.element) + (5 * o.direction);
                //o.Y = (int)Canvas.GetTop(o) + (1 * o.direction);

                if (o.X > cnv.MaxWidth || o.X < 0)
                {
                    o.direction *= -1;
                }
                Canvas.SetLeft(o.element, o.X);
            }

        }

        private void btn_Click(object sender, RoutedEventArgs e)
        {
            Window2 w2 = new Window2();
            w2.DogCreated += DogCreatedHandler; // Přihlášení k události
            w2.ShowDialog();
        }

        private void DogCreatedHandler(Dog dog)
        {
            dogList.Add(dog);
            reloadListBox();
        }
        private void reloadListBox()
        {
            foreach (var o in dogList) 
            { 
                if (!lbox.Items.Contains(o.Name)) 
                { lbox.Items.Add(o); } 
            }
        }

        private void btn2_Click(object sender, RoutedEventArgs e)
        {
            Window3 w3 = new Window3();
            w3.ShowDialog();
            dogList.Add(new Dog(w3.tbName.Text,
                Int32.Parse(w3.tbAge.Text)));
            reloadListBox();
        }

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            if(!timer.IsEnabled)
            { timer.Start(); }
            Random a = new Random();
            Elipsa el = new Elipsa(a.Next(0+20, 400-20), a.Next(0+20, 500-20));
            Canvas.SetLeft(el.element,el.X);
            Canvas.SetTop(el.element,el.Y);
            cnv.Children.Add(el.element);
            elipsList.Add(el);

        }
    }

    class Elipsa:UIElement
    {
        public Ellipse element { get; private set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int direction { get; set; }

        public Elipsa(int X, int Y, int direction=1)
        {
            element = new Ellipse()
            {
                Fill = new SolidColorBrush(Color.FromRgb(255, 0, 0)),
                Height = 20,
                Width = 20,
            };
            this.X = X;
            this.Y = Y;
            this.direction = direction;
        }

    }
}
